package com.lz.testcase.base;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@PrepareForTest(CodeUtils.class)
@RunWith(PowerMockRunner.class)
public class Demo01Test {

    @Test
    public void method01(){
        //TODO 2020/10/12 17:14
        PowerMockito.mockStatic(CodeUtils.class);
        PowerMockito.when(CodeUtils.getString()).thenReturn("liz");
        Assert.assertEquals("liz",CodeUtils.getString());

    }
}
