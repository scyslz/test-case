-- 定义分隔符 // 默认分隔符为 ；，这里定义 // ，为了告诉引擎在遇到下一个 // 的时候执行上面的操作
delimiter  //
DROP PROCEDURE testData
    delimiter ;
delimiter //
# 创建一个存储过程 testData
CREATE PROCEDURE testData()
BEGIN
    DECLARE i INT;
    set i = 1;
    WHILE (i<100000) DO
            INSERT INTO test_insert ( column_2,column_3,column_4,column_5,column_6,column_7,column_8,column_9,column_10,column_11 )
            VALUES(i,i,i,i,i,i,i,i,i,i);
            set i = i + 1;
END WHILE;
END //   # 执行上述语句

-- 恢复分隔符
delimiter ;
-- 调用存储过程
CALL testData();

create table test.test_insert
(
    id int auto_increment primary key,
    column_2 bigint null,
    column_3 int null,
    column_4 int null,
    column_5 int null,
    column_6 int null,
    column_7 int null,
    column_8 int null,
    column_9 int null,
    column_10 int null,
    column_11 int null
);


