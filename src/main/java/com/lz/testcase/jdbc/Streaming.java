package com.lz.testcase.jdbc;

import com.mysql.cj.jdbc.StatementImpl;
import com.mysql.cj.protocol.a.result.ResultsetRowsStreaming;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @description
 *
 * @date 2021-08-12 15:12
 *
 * @author Lizhong
 */
public class Streaming {

    // static String url = "jdbc:mysql://10.10.11.107:13306/scm_inventory?Unicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai";
    static String url = "jdbc:mysql://47.104.237.35/test?Unicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai";
    static String user = "root";
    static String pwd = "";
    private static String sql = "select * from test_insert  ";

    public static void selectStream() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection(url, user, pwd);
        PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        // 开启流式处理
        statement.setFetchSize(Integer.MIN_VALUE);
        ( (StatementImpl) statement).enableStreamingResults();

        long begin = System.currentTimeMillis();
        ResultSet resultSet = statement.executeQuery();
        // new ResultsetRowsStreaming()
        /*
         * "main@1" prio=5 tid=0x1 nid=NA runnable
         *   java.lang.Thread.State: RUNNABLE
         * 	  at com.mysql.cj.protocol.a.result.ResultsetRowsStreaming.<init>(ResultsetRowsStreaming.java:96)
         * 	  at com.mysql.cj.protocol.a.TextResultsetReader.read(TextResultsetReader.java:93)
         * 	  at com.mysql.cj.protocol.a.TextResultsetReader.read(TextResultsetReader.java:48)
         * 	  at com.mysql.cj.protocol.a.NativeProtocol.read(NativeProtocol.java:1601)
         * 	  at com.mysql.cj.protocol.a.NativeProtocol.readAllResults(NativeProtocol.java:1655)
         * 	  at com.mysql.cj.protocol.a.NativeProtocol.sendQueryPacket(NativeProtocol.java:964)
         * 	  at com.mysql.cj.NativeSession.execSQL(NativeSession.java:1075)
         * 	  at com.mysql.cj.jdbc.ClientPreparedStatement.executeInternal(ClientPreparedStatement.java:930)
         * 	  - locked <0x536> (a com.mysql.cj.jdbc.ConnectionImpl)
         * 	  at com.mysql.cj.jdbc.ClientPreparedStatement.executeQuery(ClientPreparedStatement.java:1003)
         * 	  at com.lz.testcase.jdbc.Streaming.selectStream(Streaming.java:36)
         * 	  at com.lz.testcase.jdbc.Streaming.main(Streaming.java:67)
         */

        while (resultSet.next()) {
            /*
         * "main@1" prio=5 tid=0x1 nid=NA runnable
          java.lang.Thread.State: RUNNABLE
              at com.mysql.cj.protocol.a.MultiPacketReader.readMessage(MultiPacketReader.java:63)
              at com.mysql.cj.protocol.a.MultiPacketReader.readMessage(MultiPacketReader.java:44)
              at com.mysql.cj.protocol.a.ResultsetRowReader.read(ResultsetRowReader.java:75)
              at com.mysql.cj.protocol.a.ResultsetRowReader.read(ResultsetRowReader.java:42)
              at com.mysql.cj.protocol.a.NativeProtocol.read(NativeProtocol.java:1588)
              at com.mysql.cj.protocol.a.result.ResultsetRowsStreaming.next(ResultsetRowsStreaming.java:193)
              at com.mysql.cj.protocol.a.result.ResultsetRowsStreaming.next(ResultsetRowsStreaming.java:62)
              at com.mysql.cj.jdbc.result.ResultSetImpl.next(ResultSetImpl.java:1778)
              - locked <0x536> (a com.mysql.cj.jdbc.ConnectionImpl)
              at com.lz.testcase.jdbc.Streaming.selectStream(Streaming.java:38)
              at com.lz.testcase.jdbc.Streaming.main(Streaming.java:52)

         */
            System.out.println(resultSet.getString(1));
        }
        long end = System.currentTimeMillis();
        System.out.println("selectStream span time=" + (end - begin) + "ms");

        resultSet.close();
        statement.close();
        connection.close();
    }


    public static void main(String[] args) throws Exception {
        selectStream();

    }
}
